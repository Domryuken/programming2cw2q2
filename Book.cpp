
#include "Book.h"
#include <iostream>

Book::Book(){}
Book::Book(string givenAuthor, string givenTitle, int givenPages)
{
    author = givenAuthor;
    title = givenTitle;
    pages = givenPages;
}
//Book::Book(int givenPages)
//{
//    author = "";
//    title = "";
//    pages = givenPages;
//}
Book::Book(const Book& orig){}
Book::~Book(){}

  
  //get methods the same across all Book objects so defined here
string Book::getAuthor(){return author;}
string Book::getTitle(){return title;}
int Book::getPages(){return pages;}
    
  //set methods the same across all Book objects so defined here
void Book::setAuthor(string givenAuthor){author = givenAuthor;}
void Book::setTitle(string givenTitle){title = givenTitle;}
void Book::setPages(int givenPages){pages = givenPages;}
    
bool Book::operator>(Book givenBook)
{
    if (author > givenBook.author)
        return true;
    else if(author == givenBook.author && title > givenBook.title)
        return true;
    else
        return false;
}  
bool Book::operator<(Book givenBook)
{
    if (author < givenBook.author)
        return true;
    else if(author == givenBook.author && title < givenBook.title)
        return true;
    else
        return false;
}  
bool Book::operator==(Book givenBook)
{
    if(author == givenBook.author && title == givenBook.title)
        return true;
}


ostream &operator<<(ostream &givenOstream, Book &givenBook)
{
    return givenOstream << givenBook.author 
            << ", \"" << givenBook.title
            << "\" (" << givenBook.pages
            << ")";
}

istream &operator>>(istream &givenIstream, Book &givenBook)
{
    string temp;
    getline(givenIstream,givenBook.author,',');
    getline(givenIstream,temp,'"');
    getline(givenIstream,givenBook.title,'"');
}


Book::operator int()
{
    return pages;
}