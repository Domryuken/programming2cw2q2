
#include "Library.h"
#include <fstream>

Library::Library(){}
Library::Library(const Library& orig){}
Library::~Library(){}

  //get methods for testing
vector <LibraryBook> &Library::getBooks(){return books;}
vector <Borrower> &Library::getBorrowers(){return borrowers;}
    
  //add book methods
void Library::addBook(LibraryBook &givenBook){books.push_back(givenBook);}
void Library::addBorrower(Borrower &givenBorrower){borrowers.push_back(givenBorrower);}


void Library::readInBooks()
{
    fstream inputFile;
    inputFile.open("librarybooks.txt");
    LibraryBook book;
    
    while(inputFile >> book && !(inputFile.eof()))
    {
        books.push_back(book);
        cout<<book<<endl;
    }
    inputFile.close();
}

void Library::readInBorrowers()
{
    fstream inputFile;
    inputFile.open("users.txt");
    Borrower borrower;
    
    while(inputFile >> borrower && !(inputFile.eof()))
    {
        borrowers.push_back(borrower);
        cout<<borrower<<endl;
    }
    inputFile.close();
}
