
#ifndef LIBRARYBOOK_H
#define	LIBRARYBOOK_H
#include "Book.h"


enum class Status{ON_LOAN=0,AVAILABLE_FOR_LENDING=1,REFERENCE_ONLY=2};

class LibraryBook : public Book
{    
public:
      //Constructors
    LibraryBook();    
    LibraryBook(string givenAuthor, string givenTitle,int givenPages,
            Status givenStatus, string givenReference);
    LibraryBook(const LibraryBook& orig);
    ~LibraryBook();
    string getStatus();
    
private: 
    Status status;
    string reference;
    

    friend ostream &operator<<(ostream &givenOstream, LibraryBook &givenBook);
    friend istream &operator>>(istream &givenIstream, LibraryBook &givenBook);
};


#endif	/* LIBRARYBOOK_H */

