
#ifndef BOOK_H
#define	BOOK_H
#include <iostream>
using namespace std;

class Book
{    
protected:
      //protected instead of private so they can be used by child class LibraryBook
    string author;
    string title;
    int pages;    
    
public:
      //constructors. Will be defined in .cpp
    Book();
    Book(string givenAuthor, string givenTitle, int givenPages);
    Book(const Book& orig);
    ~Book();
    
      //get methods the same across all Book objects so defined here
    string getAuthor();
    string getTitle();
    int getPages();
    
      //set methods the same across all Book objects so defined here
    void setAuthor(string givenAuthor);
    void setTitle(string givenTitle);
    void setPages(int givenPages);

    bool operator>(Book givenBook);
    bool operator<(Book givenBook);
    bool operator==(Book givenBook);

    friend ostream &operator<<(ostream &givenOstream, Book &givenBook);
    friend istream &operator>>(istream &givenIstream, Book &givenBook);
    
    operator int();
};

#endif	/* BOOK_H */

