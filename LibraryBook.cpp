
#include "LibraryBook.h"
#include <cstdlib>
using namespace std;


LibraryBook::LibraryBook(){}   
LibraryBook::LibraryBook(string givenAuthor, string givenTitle,int givenPages,
        Status givenStatus, string givenReference) :Book(givenAuthor,givenTitle,givenPages)
{
    status = givenStatus;
    reference = givenReference;
}
LibraryBook::LibraryBook(const LibraryBook& orig){}
LibraryBook::~LibraryBook(){}

string LibraryBook::getStatus()
{
    string statusString;
    if(status==Status::AVAILABLE_FOR_LENDING)
        statusString = "AVAILABLE FOR LENDING";
    else if(status==Status::ON_LOAN)
        statusString = "ON LOAN";
    else
        statusString = "REFERENCE ONLY";
    return statusString;
}

ostream &operator<<(ostream &givenOstream, LibraryBook &givenBook)
{

    string statusString = givenBook.getStatus();
    return givenOstream << givenBook.getAuthor()
            << ", \"" << givenBook.title
            << "\" (" << givenBook.pages
            << ") [" << givenBook.reference
            << " " << statusString
            << "]";
}           

istream &operator>>(istream &givenIstream, LibraryBook &givenBook)
{
    string temp;
    getline(givenIstream,givenBook.author,',');
    getline(givenIstream,temp,'"');
    getline(givenIstream,temp,'"');
    givenBook.setTitle(temp);
    getline(givenIstream,temp,'(');
    getline(givenIstream,temp,' ');
    givenBook.pages = atoi(temp.c_str());
    getline(givenIstream,temp,'[');
    getline(givenIstream,givenBook.reference,' ');
    getline(givenIstream,temp,']');
    if(temp=="AVAILABLE FOR LENDING")
        givenBook.status = Status::AVAILABLE_FOR_LENDING;
    else if(temp=="ON LOAN")
        givenBook.status = Status::ON_LOAN;
    else if(temp=="REFERENCE ONLY")
        givenBook.status = Status::REFERENCE_ONLY;
    getline(givenIstream,temp,'\n');
    return givenIstream;
}