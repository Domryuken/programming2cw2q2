
#ifndef LIBRARY_H
#define	LIBRARY_H
#include "LibraryBook.h"
#include "Borrower.h"
#include <vector>
using namespace std;
class Library {
private:
      //collections for the LibraryBooks and the Borrowers
    vector <LibraryBook> books;
    vector <Borrower> borrowers;
    
public:
      //Constructors
    Library();
    Library(const Library& orig);
    virtual ~Library();
    
      //get methods for testing
    vector <LibraryBook> &getBooks();
    vector <Borrower> &getBorrowers();
    
      //add book methods
    void addBook(LibraryBook &givenBook);
    void addBorrower(Borrower &givenBorrower);
    
    void readInBooks();
    void readInBorrowers();
    
};

#endif	/* LIBRARY_H */

