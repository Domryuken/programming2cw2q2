
#include <cstdlib>
#include "Borrower.h"

Borrower::Borrower(){}
Borrower::Borrower(int givenID, string givenName, string givenAddress)
{
    iD = givenID;
    name = givenName;
    address = givenAddress;
}
Borrower::Borrower(const Borrower& orig){}
Borrower::~Borrower(){}

  //getMethods
int Borrower::getID(){return iD;}
string Borrower::getName(){return name;}
string Borrower::getAddress(){return address;}
    
  //set methods
void Borrower::setID(int givenID){iD = givenID;}
void Borrower::setName(string givenName){name = givenName;}
void Borrower::setAddress(string givenAddress){address = givenAddress;}


  // I/O STREAMS
ostream &operator<<(ostream &givenOstream, Borrower &givenBorrower)
{
    return givenOstream << givenBorrower.iD
            << " " << givenBorrower.name
            << " : " << givenBorrower.address;
}
istream &operator>>(istream &givenIstream, Borrower &givenBorrower)
{
    string temp;
    getline(givenIstream,temp,' ');
    givenBorrower.iD = atoi(temp.c_str());
    getline(givenIstream,givenBorrower.name,':');
    getline(givenIstream,givenBorrower.address);
    return givenIstream;     
}