
#ifndef BORROWER_H
#define	BORROWER_H
#include <iostream>
#include <vector>
#include "LibraryBook.h"
using namespace std;

class Borrower
{
private:
      //values made private because no class needs direct access to them
    int iD;
    string name;
    string address;
    
public:
      //constructors
    Borrower();
    Borrower(int giveID, string givenName, string givenAddress);
    Borrower(const Borrower& orig);
    virtual ~Borrower();
    
      //get methods
    int getID();
    string getName();
    string getAddress();
    
      //set methods
    void setID(int givenID);
    void setName(string givenName);
    void setAddress(string givenAddress);
    
    
    friend ostream &operator<<(ostream &givenOstream, Borrower &givenBorrower);
    friend istream &operator>>(istream &givenIstream, Borrower &givenBorrower);

};

#endif	/* BORROWER_H */

